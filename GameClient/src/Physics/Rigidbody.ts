/// <reference path="../MathEx.ts" />

module Physics {
	
	import Vector2 = MathEx.Vector2;
	
	/** Represents a body governed by newtonian physics. */
	export class Rigidbody {
		
		private _position: Vector2 = new Vector2();
		/** Position. */
		public get position(): Vector2 {
			return this._position;
		}
		public set position(v: Vector2) {
			v.copyTo(this._position);
		}
		
		/** Rotation. */
		public rotation = 0;
		
		public _velocity = new Vector2();
		/** Velocity. */
		public get velocity(): Vector2 {
			return this._velocity;
		}
		public set velocity(v: Vector2) {
			v.copyTo(this._velocity);
		}
		
		/** Anglular momentum. */
		public angularMomentum = 0;
		
		/** Proceed with the simulation by the specified amount of seconds. */
		public simulate(time: number): void {
			this.position.add(this.velocity.x * time, this.velocity.y * time);
			this.rotation += this.angularMomentum;
		}
		
		/** Add a sudden force to this rigidbody (like an explosion). */
		public addForce(direction: Vector2): void;
		/** Add a continuous force to this rigidbody (like gravity). */
		public addForce(direction: Vector2, time: number): void;
		addForce(direction: Vector2, time?: number): void {
			if(arguments.length == 1)
				this.velocity.add(direction);
			else
				this.velocity.add(direction.x * time, direction.y * time);
		}
	}
}