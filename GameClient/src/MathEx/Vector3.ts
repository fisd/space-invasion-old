module MathEx {

	/** Represents a vector with two numbers. */
	export class Vector3 {
		
		/** Zero vector. */
		static origin: Vector3 = Object.freeze(new Vector3(0, 0, 0));
		
		/** Vector pointing upwards. */
		static up: Vector3 = Object.freeze(new Vector3(0, +1, 0));
		
		/** Vector pointing downwards. */
		static down: Vector3 = Object.freeze(new Vector3(0, -1, 0));
		
		/** Vector pointing left. */
		static left: Vector3 = Object.freeze(new Vector3(-1, 0, 0));
		
		/** Vector pointing right. */
		static right: Vector3 = Object.freeze(new Vector3(+1, 0, 0));
		
		/** Vector pointing frontward. */
		static front: Vector3 = Object.freeze(new Vector3(0, 0, -1));
		
		/** Vector pointing backward. */
		static back: Vector3 = Object.freeze(new Vector3(0, 0, +1));
		
		
		
		/** Create an empty vector. */
		static create(): Vector3 {
			return new Vector3();
		}
		
		/** Create a new vector initialized with the values from another. */
		static clone(source: Vector3): Vector3 {
			return Vector3.copy(Vector3.create(), source);
		}
		
		/** Copy values from a vector to another. */
		static copy(destination: Vector3, source: Vector3): Vector3 {
			for(var i=0; i<3; i++)
				destination[i] = source[i];
			
			return destination;
		}
		
		/** Multiply a vector by a scalar. */
		static multiply(destination: Vector3, v: Vector3, s: number): Vector3 {
			destination[0] = v[0] * s;
			destination[1] = v[1] * s;
			destination[2] = v[2] * s;
			return destination;
		}
		
		/** Sum two vectors. */
		static sum(destination: Vector3, a: Vector3, b: Vector3): Vector3 {
			destination[0] = a[0] + b[0];
			destination[1] = a[1] + b[1];
			destination[2] = a[2] + b[2];
			return destination;
		}
		
		/** Subtract a vector from another. */
		static subtract(destination: Vector3, a: Vector3, b: Vector3): Vector3 {
			destination[0] = a[0] - b[0];
			destination[1] = a[1] - b[1];
			destination[2] = a[2] - b[2];
			return destination;
		}
		
		/** Compute the dot product between two vectors. */
		static dot(a: Vector3, b: Vector3): number {
			return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
		}
		
		/** Normalize a vector. */
		static normalize(destination: Vector3, source: Vector3): Vector3 {
			Vector3.copy(destination, source);
			return Vector3.multiply(destination, destination, Vector3.magnitude(destination));
		}
		
		/** Compute the magnitude of a vector (slow, prefer magnitudeSquare when possible). */
		static magnitude(source: Vector3): number {
			return Math.sqrt(Vector3.squareMagnitude(source));
		}
		
		/** Compute the square of the magnitude of a vector. */
		static squareMagnitude(source: Vector3): number {
			return source[0] * source[0] + source[1] * source[1] + source[2] * source[2];
		}
		
		
		
		/** X value. */
		public get x(): number { return this[0]; }
		public set x(v: number) { this[0] = v; }
		public 0: number;
		
		/** Y value. */
		public get y(): number { return this[1]; }
		public set y(v: number) { this[1] = v; }
		public 1: number;
		
		/** Z value. */
		public get z(): number { return this[2]; }
		public set z(v: number) { this[2] = v; }
		public 2: number;
		
		/** Construct an empty vector. */
		public constructor();
		/** Construct a vector. */
		public constructor(x: number, y: number, z: number);
		constructor(x?: number, y?: number, z?: number) {
			this.x = x || 0;
			this.y = y || 0;
			this.z = z || 0;
		}
		
		/** Create a new vector initialized with the values from this one. */
		public clone(): Vector3 {
			return Vector3.clone(this);
		}
		
		/** Copy values from this vector to another. */
		public copyTo(destination: Vector3): Vector3 {
			Vector3.copy(destination, this);
			return this;
		}
		
		/** Multiply this vector with a scalar. */
		public multiplyToBy(destination: Vector3, scalar: number): Vector3 {
			Vector3.multiply(destination, this, scalar);
			return this;
		}
		
		/** Multiply this vector with a scalar. */
		public multiplyBy(scalar: number): Vector3 {
			return Vector3.multiply(this, this, scalar);
		}
		
		/** Sum this vector and another. */
		public plus(destination: Vector3, other: Vector3): Vector3 {
			Vector3.sum(destination, this, other);
			return this;
		}
		
		/** Add a vector to this one. */
		public add(x: number, y: number, z: number): Vector3;
		public add(other: Vector3): Vector3;
		add(otherOrX: Vector3|number, y?: number, z?: number): Vector3 {
			if(arguments.length == 1) {
				return Vector3.sum(this, this, <Vector3>otherOrX);
			} else {
				this[0] += <number>otherOrX;
				this[1] += y;
				this[2] += z;
				return this;
			}
		}
		
		/** Subtract a vector from this one. */
		public subtract(other: Vector3): Vector3 {
			return Vector3.subtract(this, this, other);
		}
		
		/** Get the result of subtracting a vector from this one. */
		public minus(destination: Vector3, other: Vector3): Vector3 {
			Vector3.subtract(destination, this, other);
			return this;
		}
		
		/** Compute the dot product between this vector and another. */
		public dot(other: Vector3): number {
			return Vector3.dot(this, other);
		}
		
		/** Normalize this vector. */
		public normalize(): Vector3 {
			return Vector3.normalize(this, this);
		}
		
		/** Normalize this vector. */
		public normalizeTo(destination: Vector3): Vector3 {
			return Vector3.normalize(destination, this);
		}
		
		/** Magnitude of this vector (slow, prefer magnitudeSquare when possible). */
		public get magnitude(): number {
			return Vector3.magnitude(this);
		}
		public set magnitude(v: number) {
			this.multiplyBy(v / this.magnitude);
		}
		
		/** The square of the magnitude of this vector. */
		public get magnitudeSquare(): number {
			return Vector3.squareMagnitude(this);
		}
	}
}