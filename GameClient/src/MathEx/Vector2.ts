module MathEx {

	/** Represents a vector with two numbers. */
	export class Vector2 {
		
		/** Zero vector. */
		static origin: Vector2 = Object.freeze(new Vector2(0, 0));
		
		/** Vector pointing upwards. */
		static up: Vector2 = Object.freeze(new Vector2(0, +1));
		
		/** Vector pointing downwards. */
		static down: Vector2 = Object.freeze(new Vector2(0, -1));
		
		/** Vector pointing left. */
		static left: Vector2 = Object.freeze(new Vector2(-1, 0));
		
		/** Vector pointing right. */
		static right: Vector2 = Object.freeze(new Vector2(+1, 0));
		
		
		
		/** Create an empty vector. */
		static create(): Vector2 {
			return new Vector2();
		}
		
		/** Create a new vector initialized with the values from another. */
		static clone(source: Vector2): Vector2 {
			return Vector2.copy(Vector2.create(), source);
		}
		
		/** Copy values from a vector to another. */
		static copy(destination: Vector2, source: Vector2): Vector2 {
			for(var i=0; i<2; i++)
				destination[i] = source[i];
			
			return destination;
		}
		
		/** Multiply a vector by a scalar. */
		static multiply(destination: Vector2, v: Vector2, s: number): Vector2 {
			destination[0] = v[0] * s;
			destination[1] = v[1] * s;
			return destination;
		}
		
		/** Sum two vectors. */
		static sum(destination: Vector2, a: Vector2, b: Vector2): Vector2 {
			destination[0] = a[0] + b[0];
			destination[1] = a[1] + b[1];
			return destination;
		}
		
		/** Subtract a vector from another. */
		static subtract(destination: Vector2, a: Vector2, b: Vector2): Vector2 {
			destination[0] = a[0] - b[0];
			destination[1] = a[1] - b[1];
			return destination;
		}
		
		/** Compute the dot product between two vectors. */
		static dot(a: Vector2, b: Vector2): number {
			return a[0]*b[0] + a[1]*b[1];
		}
		
		/** Normalize a vector. */
		static normalize(destination: Vector2, source: Vector2): Vector2 {
			Vector2.copy(destination, source);
			return Vector2.multiply(destination, destination, Vector2.magnitude(destination));
		}
		
		/** Compute the magnitude of a vector (slow, prefer magnitudeSquare when possible). */
		static magnitude(source: Vector2): number {
			return Math.sqrt(Vector2.squareMagnitude(source));
		}
		
		/** Compute the square of the magnitude of a vector. */
		static squareMagnitude(source: Vector2): number {
			return source[0] * source[0] + source[1] * source[1];
		}
		
		
		
		/** X value. */
		public get x(): number { return this[0]; }
		public set x(v: number) { this[0] = v; }
		public 0: number;
		
		/** Y value. */
		public get y(): number { return this[1]; }
		public set y(v: number) { this[1] = v; }
		public 1: number;
		
		/** Construct an empty vector. */
		public constructor();
		/** Construct a vector. */
		public constructor(x: number, y: number);
		constructor(x?: number, y?: number) {
			this.x = x || 0;
			this.y = y || 0;
		}
		
		/** Create a new vector initialized with the values from this one. */
		public clone(): Vector2 {
			return Vector2.clone(this);
		}
		
		/** Copy values from this vector to another. */
		public copyTo(destination: Vector2): Vector2 {
			Vector2.copy(destination, this);
			return this;
		}
		
		/** Multiply this vector with a scalar. */
		public multiplyToBy(destination: Vector2, scalar: number): Vector2 {
			Vector2.multiply(destination, this, scalar);
			return this;
		}
		
		/** Multiply this vector with a scalar. */
		public multiplyBy(scalar: number): Vector2 {
			return Vector2.multiply(this, this, scalar);
		}
		
		/** Sum this vector and another. */
		public plus(destination: Vector2, other: Vector2): Vector2 {
			Vector2.sum(destination, this, other);
			return this;
		}
		
		/** Add a vector to this one. */
		public add(x: number, y: number): Vector2;
		public add(other: Vector2): Vector2;
		add(otherOrX: Vector2|number, y?: number): Vector2 {
			if(arguments.length == 1) {
				return Vector2.sum(this, this, <Vector2>otherOrX);
			} else {
				this[0] += <number>otherOrX;
				this[1] += y;
				return this;
			}
		}
		
		/** Subtract a vector from this one. */
		public subtract(other: Vector2): Vector2 {
			return Vector2.subtract(this, this, other);
		}
		
		/** Get the result of subtracting a vector from this one. */
		public minus(destination: Vector2, other: Vector2): Vector2 {
			Vector2.subtract(destination, this, other);
			return this;
		}
		
		/** Compute the dot product between this vector and another. */
		public dot(other: Vector2): number {
			return Vector2.dot(this, other);
		}
		
		/** Normalize this vector. */
		public normalize(): Vector2 {
			return Vector2.normalize(this, this);
		}
		
		/** Normalize this vector. */
		public normalizeTo(destination: Vector2): Vector2 {
			return Vector2.normalize(destination, this);
		}
		
		/** Magnitude of this vector (slow, prefer magnitudeSquare when possible). */
		public get magnitude(): number {
			return Vector2.magnitude(this);
		}
		public set magnitude(v: number) {
			this.multiplyBy(v / this.magnitude);
		}
		
		/** The square of the magnitude of this vector. */
		public get magnitudeSquare(): number {
			return Vector2.squareMagnitude(this);
		}
	}
}