module MathEx {
	
	var identityArray = [
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	];
	
	//var buffer: Matrix4 = <any>newIdentityMatrix();
	
	/** Represents a 4x4 matrix of floats. */
	export class Matrix4 {
		
		/** Create an identity matrix. */
		static create(): Matrix4 {
			var m: Matrix4 = <any>new Float32Array(identityArray);
			
			for(var k in Matrix4.prototype)
				m[k] = Matrix4.prototype[k];
			
			return m;	// this is hacky...
		}

		/** Create a new matrix initialized with the values from another. */
		static clone(source: Matrix4): Matrix4 {
			return Matrix4.copy(Matrix4.create(), source);
		}
		
		/** Copy values from a matrix to another. */
		static copy(destination: Matrix4, source: Matrix4): Matrix4 {
			for(var i=0; i<16; i++)
				destination[i] = source[i];
			
			return destination;
		}
		
		/** Set a matrix to identity matrix. */
		static identity(destination: Matrix4): Matrix4 {
			return Matrix4.copy(destination, <any>identityArray);
		}
		
		/** Transpose values of a matrix. */
		static transpose(destination: Matrix4, source: Matrix4): Matrix4 {
			if(destination === source) {
				var v01 = source[ 1];
				var v02 = source[ 2];
				var v03 = source[ 3];
				var v12 = source[ 6];
				var v13 = source[ 7];
				var v23 = source[11];
				
				destination[ 1] = source[ 4];
				destination[ 2] = source[ 8];
				destination[ 3] = source[12];
				destination[ 4] = v01;
				destination[ 6] = source[ 9];
				destination[ 7] = source[13];
				destination[ 8] = v02;
				destination[ 9] = v12;
				destination[11] = source[14];
				destination[12] = v03;
				destination[13] = v13;
				destination[14] = v23;
			} else {
				destination[ 0] = source[ 0];
				destination[ 1] = source[ 4];
				destination[ 2] = source[ 8];
				destination[ 3] = source[12];
				destination[ 4] = source[ 1];
				destination[ 5] = source[ 5];
				destination[ 6] = source[ 9];
				destination[ 7] = source[13];
				destination[ 8] = source[ 2];
				destination[ 9] = source[ 6];
				destination[10] = source[10];
				destination[11] = source[14];
				destination[12] = source[ 3];
				destination[13] = source[ 7];
				destination[14] = source[11];
				destination[15] = source[15];
			}
			
			return destination;
		}
		
		/** Invert a matrix. */
		static invert(destination: Matrix4, source: Matrix4): Matrix4 {
			var b00 = source[ 0] * source[ 5] - source[ 1] * source[ 4];
			var b01 = source[ 0] * source[ 6] - source[ 2] * source[ 4];
			var b02 = source[ 0] * source[ 7] - source[ 3] * source[ 4];
			var b03 = source[ 1] * source[ 6] - source[ 2] * source[ 5];
			var b04 = source[ 1] * source[ 7] - source[ 3] * source[ 5];
			var b05 = source[ 2] * source[ 7] - source[ 3] * source[ 6];
			var b06 = source[ 8] * source[13] - source[ 9] * source[12];
			var b07 = source[ 8] * source[14] - source[10] * source[12];
			var b08 = source[ 8] * source[15] - source[11] * source[12];
			var b09 = source[ 9] * source[14] - source[10] * source[13];
			var b10 = source[ 9] * source[15] - source[11] * source[13];
			var b11 = source[10] * source[15] - source[11] * source[14];
			
			var inverseDeterminant = 1 / Matrix4.determinant(source);
			
			destination[ 0] = (source[ 5] * b11 - source[ 6] * b10 + source[ 7] * b09) * inverseDeterminant;
			destination[ 1] = (source[ 2] * b10 - source[ 1] * b11 - source[ 3] * b09) * inverseDeterminant;
			destination[ 2] = (source[13] * b05 - source[14] * b04 + source[15] * b03) * inverseDeterminant;
			destination[ 3] = (source[10] * b04 - source[ 9] * b05 - source[11] * b03) * inverseDeterminant;
			destination[ 4] = (source[ 6] * b08 - source[ 4] * b11 - source[ 7] * b07) * inverseDeterminant;
			destination[ 5] = (source[ 0] * b11 - source[ 2] * b08 + source[ 3] * b07) * inverseDeterminant;
			destination[ 6] = (source[14] * b02 - source[12] * b05 - source[15] * b01) * inverseDeterminant;
			destination[ 7] = (source[ 8] * b05 - source[10] * b02 + source[11] * b01) * inverseDeterminant;
			destination[ 8] = (source[ 4] * b10 - source[ 5] * b08 + source[ 7] * b06) * inverseDeterminant;
			destination[ 9] = (source[ 1] * b08 - source[ 0] * b10 - source[ 3] * b06) * inverseDeterminant;
			destination[10] = (source[12] * b04 - source[13] * b02 + source[15] * b00) * inverseDeterminant;
			destination[11] = (source[ 9] * b02 - source[ 8] * b04 - source[11] * b00) * inverseDeterminant;
			destination[12] = (source[ 5] * b07 - source[ 4] * b09 - source[ 6] * b06) * inverseDeterminant;
			destination[13] = (source[ 0] * b09 - source[ 1] * b07 + source[ 2] * b06) * inverseDeterminant;
			destination[14] = (source[13] * b01 - source[12] * b03 - source[14] * b00) * inverseDeterminant;
			destination[15] = (source[ 8] * b03 - source[ 9] * b01 + source[10] * b00) * inverseDeterminant;
			
			return destination;
		}
		
		/** Compute the determinant of a matrix. */
		static determinant(matrix: Matrix4): number {
			return
				(matrix[ 0] * matrix[ 5] - matrix[ 1] * matrix[ 4]) * (matrix[10] * matrix[15] - matrix[11] * matrix[14]) -
				(matrix[ 0] * matrix[ 6] - matrix[ 2] * matrix[ 4]) * (matrix[ 9] * matrix[15] - matrix[11] * matrix[13]) +
				(matrix[ 0] * matrix[ 7] - matrix[ 3] * matrix[ 4]) * (matrix[ 9] * matrix[14] - matrix[10] * matrix[13]) +
				(matrix[ 1] * matrix[ 6] - matrix[ 2] * matrix[ 5]) * (matrix[ 8] * matrix[15] - matrix[11] * matrix[12]) -
				(matrix[ 1] * matrix[ 7] - matrix[ 3] * matrix[ 5]) * (matrix[ 8] * matrix[14] - matrix[10] * matrix[12]) +
				(matrix[ 2] * matrix[ 7] - matrix[ 3] * matrix[ 6]) * (matrix[ 8] * matrix[13] - matrix[ 9] * matrix[12]);
		}
		
		/** Multiply two matrices. */
		static multiply(destination: Matrix4, a: Matrix4, b: Matrix4): Matrix4 {
			var a00 = a[ 0], a01 = a[ 1], a02 = a[ 2], a03 = a[ 3],
			    a10 = a[ 4], a11 = a[ 5], a12 = a[ 6], a13 = a[ 7],
			    a20 = a[ 8], a21 = a[ 9], a22 = a[10], a23 = a[11],
			    a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];
			
			var b0 = b[ 0], b1 = b[ 1], b2 = b[ 2], b3 = b[ 3];
			destination[ 0] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
			destination[ 1] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
			destination[ 2] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
			destination[ 3] = b0*a03 + b1*a13 + b2*a23 + b3*a33;
			
			    b0 = b[ 4]; b1 = b[ 5]; b2 = b[ 6]; b3 = b[ 7];
			destination[ 4] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
			destination[ 5] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
			destination[ 6] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
			destination[ 7] = b0*a03 + b1*a13 + b2*a23 + b3*a33;
			
			    b0 = b[ 8]; b1 = b[ 9]; b2 = b[10]; b3 = b[11];
			destination[ 8] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
			destination[ 9] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
			destination[10] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
			destination[11] = b0*a03 + b1*a13 + b2*a23 + b3*a33;
			
			    b0 = b[12]; b1 = b[13]; b2 = b[14]; b3 = b[15];
			destination[12] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
			destination[13] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
			destination[14] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
			destination[15] = b0*a03 + b1*a13 + b2*a23 + b3*a33;
			
			//if(destination === a || destination === b) {
			//	if(destination === a && destination === b)
			//		a = b = Matrix4.copy(buffer, destination);
			//	else if(destination === a)
			//		a = Matrix4.copy(buffer, destination);
			//	else
			//		b = Matrix4.copy(buffer, destination);
			//}
			//
			//for(var y=0; y<4; y++)
			//	for(var x=0; x<4; x++)
			//		destination[y*4+x] =
			//			b[y*4+ 0] * a[ 0+x] +
			//			b[y*4+ 1] * a[ 4+x] +
			//			b[y*4+ 2] * a[ 8+x] +
			//			b[y*4+ 3] * a[12+x];
			
			return destination;
		}
		
		/** Translate a matrix by the specified amount. */
		static translate(destination: Matrix4, source: Matrix4, translation: Vector3): Matrix4 {
			if(source !== destination)
				Matrix4.copy(destination, source);
			
			var x = translation[0], y = translation[1], z = translation[2];
			destination[12] = source[ 0] * x + source[ 4] * y + source[ 8] * z + source[12];
			destination[13] = source[ 1] * x + source[ 5] * y + source[ 9] * z + source[13];
			destination[14] = source[ 2] * x + source[ 6] * y + source[10] * z + source[14];
			destination[15] = source[ 3] * x + source[ 7] * y + source[11] * z + source[15];
			
			return destination;
		}
		
		/** Rotates a matrix around the X axis. */
		static rotateX(destination: Matrix4, source: Matrix4, rotationSin: number, rotationCos: number): Matrix4 {
			var v10 = source[ 4];
			var v11 = source[ 5];
			var v12 = source[ 6];
			var v13 = source[ 7];
			var v20 = source[ 8];
			var v21 = source[ 9];
			var v22 = source[10];
			var v23 = source[11];
			
			if(source !== destination) {
				destination[ 0] = source[ 0];
				destination[ 1] = source[ 1];
				destination[ 2] = source[ 2];
				destination[ 3] = source[ 3];
				destination[12] = source[12];
				destination[13] = source[13];
				destination[14] = source[14];
				destination[15] = source[15];
			}
			
			destination[ 4] = v10 * rotationCos + v20 * rotationSin;
			destination[ 5] = v11 * rotationCos + v21 * rotationSin;
			destination[ 6] = v12 * rotationCos + v22 * rotationSin;
			destination[ 7] = v13 * rotationCos + v23 * rotationSin;
			destination[ 8] = v20 * rotationCos - v10 * rotationSin;
			destination[ 9] = v21 * rotationCos - v11 * rotationSin;
			destination[10] = v22 * rotationCos - v12 * rotationSin;
			destination[11] = v23 * rotationCos - v13 * rotationSin;
			
			return destination;
		}
		
		/** Rotates a matrix around the Y axis. */
		static rotateY(destination: Matrix4, source: Matrix4, rotationSin: number, rotationCos: number): Matrix4 {
			var a00 = source[ 0];
			var a01 = source[ 1];
			var a02 = source[ 2];
			var a03 = source[ 3];
			var a20 = source[ 8];
			var a21 = source[ 9];
			var a22 = source[10];
			var a23 = source[11];
			
			if(source !== destination) {
				destination[ 4] = source[ 4];
				destination[ 5] = source[ 5];
				destination[ 6] = source[ 6];
				destination[ 7] = source[ 7];
				destination[12] = source[12];
				destination[13] = source[13];
				destination[14] = source[14];
				destination[15] = source[15];
			}
			
			destination[ 0] = a00 * rotationCos - a20 * rotationSin;
			destination[ 1] = a01 * rotationCos - a21 * rotationSin;
			destination[ 2] = a02 * rotationCos - a22 * rotationSin;
			destination[ 3] = a03 * rotationCos - a23 * rotationSin;
			destination[ 8] = a00 * rotationSin + a20 * rotationCos;
			destination[ 9] = a01 * rotationSin + a21 * rotationCos;
			destination[10] = a02 * rotationSin + a22 * rotationCos;
			destination[11] = a03 * rotationSin + a23 * rotationCos;
			
			return destination;
		}
		
		/** Rotates a matrix around the Z axis. */
		static rotateZ(destination: Matrix4, source: Matrix4, rotationSin: number, rotationCos: number) {
			var a00 = source[ 0];
			var a01 = source[ 1];
			var a02 = source[ 2];
			var a03 = source[ 3];
			var a10 = source[ 4];
			var a11 = source[ 5];
			var a12 = source[ 6];
			var a13 = source[ 7];
			
			if(source !== destination) {
				destination[ 8] = source[ 8];
				destination[ 9] = source[ 9];
				destination[10] = source[10];
				destination[11] = source[11];
				destination[12] = source[12];
				destination[13] = source[13];
				destination[14] = source[14];
				destination[15] = source[15];
			}
			
			destination[ 0] = a00 * rotationCos + a10 * rotationSin;
			destination[ 1] = a01 * rotationCos + a11 * rotationSin;
			destination[ 2] = a02 * rotationCos + a12 * rotationSin;
			destination[ 3] = a03 * rotationCos + a13 * rotationSin;
			destination[ 4] = a10 * rotationCos - a00 * rotationSin;
			destination[ 5] = a11 * rotationCos - a01 * rotationSin;
			destination[ 6] = a12 * rotationCos - a02 * rotationSin;
			destination[ 7] = a13 * rotationCos - a03 * rotationSin;
			
			return destination;
		}
		
		/** Create a perspective matrix. */
		static perspective = function(destination: Matrix4, fovy: number, aspect: number, near: number, far: number): Matrix4 {
			Matrix4.identity(destination);
			
			var f  = 1 / Math.tan(fovy / 2);
			var nf = 1 / (near - far);
			
			destination[0] = f / aspect;
			destination[5] = f;
			destination[10] = (far + near) * nf;
			destination[11] = -1;
			destination[14] = (2 * far * near) * nf;
			destination[15] = 0;
			
			return destination;
		};
		
		
		
		/** Construct an identity matrix. */
		public constructor() {
			return Matrix4.create();	// mmh... too hacky?
		}
		
		/** Create a new matrix initialized with the values from this one. */
		public clone(): Matrix4 {
			return Matrix4.clone(this);
		}
		
		/** Copy values from this matrix to another. */
		public copyTo(destination: Matrix4): Matrix4 {
			Matrix4.copy(destination, this);
			return this;
		}
		
		/** Set this matrix to identity matrix. */
		public toIdentity(): Matrix4 {
			return Matrix4.identity(this);
		}
		
		/** Transpose values of this matrix. */
		public transpose(): Matrix4 {
			return this.transposeTo(this);
		}
		
		/** Transpose values of this matrix. */
		public transposeTo(destination: Matrix4): Matrix4 {
			Matrix4.transpose(destination, this);
			return this;
		}
		
		/** Invert this matrix. */
		public invert(): Matrix4 {
			return this.invertTo(this);
		}
		
		/** Invert this matrix. */
		public invertTo(destination: Matrix4): Matrix4 {
			Matrix4.invert(destination, this);
			return this;
		}
		
		/** Compute the determinant of this matrix. */
		public getDeterminant(matrix: Matrix4): number {
			return Matrix4.determinant(matrix);
		}
		
		/** Multiply this matrix with another. */
		public multiplyToBy(destination: Matrix4, other: Matrix4): Matrix4 {
			Matrix4.multiply(destination, this, other);
			return this;
		}
		
		/** Multiply this matrix by another. */
		public multiplyBy(other: Matrix4): Matrix4 {
			return Matrix4.multiply(this, this, other);
		}
		
		/** Translate this matrix by a vector. */
		public translateToBy(destination: Matrix4, translate: Vector3): Matrix4 {
			Matrix4.translate(destination, this, translate);
			return this;
		}
		
		/** Translate this matrix by a vector. */
		public translateBy(translate: Vector3): Matrix4 {
			return Matrix4.translate(this, this, translate);
		}
		
		/** Rotate this matrix around the X axis. */
		public rotateX(rotationSin: number, rotationCos: number): Matrix4 {
			return Matrix4.rotateX(this, this, rotationSin, rotationCos);
		}
		
		/** Rotate this matrix around the X axis. */
		public rotateXTo(destination: Matrix4, rotationSin: number, rotationCos: number): Matrix4 {
			Matrix4.rotateX(destination, this, rotationSin, rotationCos);
			return this;
		}
		
		/** Rotate this matrix around the Y axis. */
		public rotateY(rotationSin: number, rotationCos: number): Matrix4 {
			return Matrix4.rotateY(this, this, rotationSin, rotationCos);
		}
		
		/** Rotate this matrix around the Y axis. */
		public rotateYTo(destination: Matrix4, rotationSin: number, rotationCos: number): Matrix4 {
			Matrix4.rotateY(destination, this, rotationSin, rotationCos);
			return this;
		}
		
		/** Rotate this matrix around the Z axis. */
		public rotateZ(rotationSin: number, rotationCos: number): Matrix4 {
			return Matrix4.rotateZ(this, this, rotationSin, rotationCos);
		}
		
		/** Rotate this matrix around the Z axis. */
		public rotateZTo(destination: Matrix4, rotationSin: number, rotationCos: number): Matrix4 {
			Matrix4.rotateZ(destination, this, rotationSin, rotationCos);
			return this;
		}
		
		/** Set this matrix to perspective matrix. */
		public toPerspective(fovy: number, aspect: number, near: number, far: number): Matrix4 {
			return Matrix4.perspective(this, fovy, aspect, near, far);
		}
		
	}
}