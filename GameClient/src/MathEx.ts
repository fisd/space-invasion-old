/// <reference path="MathEx/Matrix4.ts" />
/// <reference path="MathEx/Vector2.ts" />
/// <reference path="MathEx/Vector3.ts" />

/** Provides mathematical utilities not included with the standard framework. */
module MathEx {
	
	/** Square root of 3. */
	export var SQRT3 = Math.sqrt(3);
	
	/** Clamp a number between a minimum and a maximum. */
	export function clamp(min: number, max: number, v: number) {
		return Math.max(min, Math.min(max, v));
	}
	
	/** Linear interpolation between two values, at position t. */
	export function lerp(min: number, max: number, t: number): number {
		if(t < 0)
			t = 0;
		else if(t > 1)
			t = 1;
		
		return min + t*(max-min);
	}
}
