/// <reference path="Graphics/Attribute.ts" />
/// <reference path="Graphics/Canvas.ts" />
/// <reference path="Graphics/Color.ts" />
/// <reference path="Graphics/ImageSprite.ts" />
/// <reference path="Graphics/Input.ts" />
/// <reference path="Graphics/Materials.ts" />
/// <reference path="Graphics/Mesh.ts" />
/// <reference path="Graphics/Program.ts" />
/// <reference path="Graphics/Shader.ts" />
/// <reference path="Graphics/Sprite.ts" />
/// <reference path="Graphics/TextSprite.ts" />
/// <reference path="Graphics/Uniform.ts" />

/** Contains graphics and rendering-related utilities. */
module Graphics {
	
	/** Represents the horizontal alignment. */
	export enum HorizontalAlignment {
		left   = -1,
		center =  0,
		right  = +1
	}
	
	/** Represents the vertical alignment. */
	export enum VerticalAlignment {
		top    = -1,
		middle =  0,
		bottom = +1
	}
}
