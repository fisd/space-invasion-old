module Graphics {
	
	/** Represents a WebGL program. */
	export class Program {
	
		private canvas: Canvas;
		private gl: WebGLRenderingContext;
		/** INTERNAL PROPERTY, DO NOT USE */
		public _program: WebGLProgram;
		
		/** Construct a program. */
		public constructor(canvas: Canvas, shaders: Shader[]) {
			this.canvas = canvas;
			var gl = this.gl = canvas.webgl;
			
			this._program = gl.createProgram();
			
			for(var i=0; i<shaders.length; i++) {
				gl.attachShader(this._program, shaders[i]._getShader());
			}
			
			gl.linkProgram(this._program);
			
			if(!gl.getProgramParameter(this._program, gl.LINK_STATUS)) {
				console.error(gl.getProgramInfoLog(this._program));
				return null;
			}
		}
		
		/** Indicate WebGL to use this program. */
		public use(): void {
			this.gl.useProgram(this._program);
		}
	}
}