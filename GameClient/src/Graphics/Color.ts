/// <reference path="../MathEx.ts" />

module Graphics {

	import Matrix4 = MathEx.Matrix4;
	
	/** Represents a color. */
	export class Color {
		
		public static black   = Color.fromComponents(  0,   0,   0);
		public static red     = Color.fromComponents(255,   0,   0);
		public static green   = Color.fromComponents(  0, 255,   0);
		public static yellow  = Color.fromComponents(255, 255,   0);
		public static blue    = Color.fromComponents(  0,   0, 255);
		public static magenta = Color.fromComponents(255,   0, 255);
		public static cyan    = Color.fromComponents(  0, 255, 255);
		public static white   = Color.fromComponents(255, 255, 255);
		
		/** Red value, between 0 and 255. */
		public red: number = 0;
		
		/** Green value, between 0 and 255. */
		public green: number = 0;
		
		/** Blue value, between 0 and 255. */
		public blue: number = 0;
		
		/** Alpha value, between 0 and 255. */
		public alpha: number = 255;
		
		/** Construct a color from components between 0 and 255. */
		public static fromComponents(r: number, g: number, b: number) : Color;
		
		/** Construct a color from components between 0 and 255. */
		public static fromComponents(r: number, g: number, b: number, a: number) : Color;
		
		static fromComponents(r: number, g: number, b: number, a?: number) : Color {
			var c = new Color();
			c.red = r;
			c.green = g;
			c.blue = b;
			
			if(arguments.length == 4)
				c.alpha = a;
			
			return c;
		}
		
		/** Convert this color to string. */
		public toString(): string {
			if(this.alpha < 255) {
				if(!this.alpha)
					return 'transparent';
				
				return 'rgba('+this.red+', '+this.green+', '+this.blue+', '+(Math.round(this.alpha*10e4/255)/10e4)+')';
			}
			
			return 'rgb('+this.red+', '+this.green+', '+this.blue+')';
		}
		
		private static buffer = new Matrix4();
		
		/** Generate a matrix to change hue by the specified angle in radians. */
		public static computeHueRotationMatrix(rot: number): Matrix4 {
			// http://www.graficaobscura.com/matrix/index.html
			
			// === DON'T PRESERVE LUMINOSITY ===
			var m = new Matrix4();
			
			// rotate the grey vector into positive Z
			m.rotateZ( 1/Math.SQRT2  , 1/Math.SQRT2           )
			 .rotateY(-1/MathEx.SQRT3, Math.SQRT2/MathEx.SQRT3);
			
			// rotate the hue
			m.rotateX( Math.sin(rot) , Math.cos(rot)          );
			
			// rotate the grey vector back into place
			m.rotateY( 1/MathEx.SQRT3, Math.SQRT2/MathEx.SQRT3)
			 .rotateZ(-1/Math.SQRT2  , 1/Math.SQRT2           );
			
			
			
			// === PRESERVE LUMINOSITY (bugged) ===
			//var m = new Matrix4();
			//var b = Color.buffer.toIdentity();
			//
			//// rotate the grey vector into positive Z
			//m.rotateZ( 1/Math.SQRT2, 1/Math.SQRT2);
			//m.rotateY(-1/MathEx.SQRT3, Math.SQRT2/MathEx.SQRT3);
			//
			//// shear the space to make the luminance plane horizontal
			//var RLUM = 0.3086;
			//var GLUM = 0.6094;
			//var BLUM = 0.0820;
			//
			//var lx = RLUM*m[ 0] + GLUM*m[ 1] + BLUM*m[ 2] + m[ 3];
			//var ly = RLUM*m[ 4] + GLUM*m[ 5] + BLUM*m[ 6] + m[ 7];
			//var lz = RLUM*m[ 8] + GLUM*m[ 9] + BLUM*m[10] + m[11];
			//
			//b[8] = lx/lz;
			//b[9] = ly/lz;
			//m.multiplyBy(b);
			//
			//// rotate the hue
			//m.rotateX(Math.sin(rot), Math.cos(rot));
			//
			//// unshear the space to put the luminance plane back
			//b[8] = -lx/lz;
			//b[9] = -ly/lz;
			//m.multiplyBy(b);
			//
			//// rotate the grey vector back into place
			//m.rotateY( 1/MathEx.SQRT3, Math.SQRT2/MathEx.SQRT3);
			//m.rotateZ(-1/Math.SQRT2, 1/Math.SQRT2);
			
			return m;
		}
		
		/** Apply a matrix transformation to this color. */
		public applyMatrix(matrix: Matrix4): void {
			var r = this.red*matrix[ 0] + this.green*matrix[ 4] + this.blue*matrix[ 8] + matrix[12];
			var g = this.red*matrix[ 1] + this.green*matrix[ 5] + this.blue*matrix[ 9] + matrix[13];
			var b = this.red*matrix[ 2] + this.green*matrix[ 6] + this.blue*matrix[10] + matrix[14];
			
			this.red   = r;
			this.green = g;
			this.blue  = b;
		}
	}
}