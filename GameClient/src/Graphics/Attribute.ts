module Graphics {
	
	/** Represents a WebGL attribute. */
	export class Attribute {
		
		private canvas: Canvas;
		private gl: WebGLRenderingContext;
		private program: WebGLProgram;
		
		private location: number;
		
		/** Extract the specified attribute from the specified program. */
		public constructor(canvas: Canvas, program: Program, name: string) {
			this.canvas = canvas;
			var gl = this.gl = canvas.webgl;
			this.program = program;
			
			this.location = gl.getAttribLocation(program._program, name);
		}
		
		/** Enable this attribute. */
		public enable(): void {
			this.gl.enableVertexAttribArray(this.location);
		}
		
		/** Disable this attribute. */
		public disable(): void {
			this.gl.disableVertexAttribArray(this.location);
		}
		
		/** INTERNAL FUNCTION, DO NOT USE */
		public _setPointer(size: number, type: number, normalized: boolean, stride: number, offset: number) {
			this.gl.vertexAttribPointer(this.location, size, type, normalized, stride, offset);
		}
	}
}