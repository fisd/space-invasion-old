module Graphics {
	
	/** Represents a renderizable sprite. */
	export class ImageSprite implements Sprite {
		
		private asImage: HTMLImageElement;
		private asCanvas: HTMLCanvasElement;
		
		private canvas: Canvas;
		
		/** True if the sprite is ready to be rendered (readonly). */
		public complete: boolean = false;
		
		/** True if the sprite will never be rendered (readonly). */
		public failed: boolean = false;
		
		/** Pivot X coordinate. */
		public pivotX: number = 0.5;
		/** Pivot Y coordinate. */
		public pivotY: number = 0.5;
		
		/** Defines if the image should be hinted to be aligned with screen pixels. */
		public hint: boolean = false;
		
		/** Construct a sprite to be rendered in canvas, containing the specified image. */
		public constructor(canvas: Canvas, img: HTMLImageElement) {
			this.canvas = canvas;
			
			this.asImage = <HTMLImageElement>$('<img>')
				.load(() => this.imageLoaded())
				.error(() => this.imageError())
				.prop('src', img.src)
				.get(0);
		}
		
		/** Render this sprite at the specified coordinates. */
		public render(x: number, y: number): void;
		
		/** Render this sprite with the specified coordinates and rotation in radians. */
		public render(x: number, y: number, rotation: number): void;
		
		/** Render this sprite with the specified coordinates and dimension. */
		public render(x: number, y: number, width: number, height: number): void;
		
		/** Render this sprite with the specified coordinates, dimension, and rotation in radians. */
		public render(x: number, y: number, width: number, height: number, rotation: number): void;
		
		render(x: number, y: number, widthOrRotation?: number, height?: number, rotation?: number): void {
			var width = <number>undefined;
			if(arguments.length == 3)
				rotation = widthOrRotation;
			else
				width = widthOrRotation;
			
			var ctx = this.canvas.context2d;
			var texture = this.asCanvas || this.asImage;
			
			var widthAndHeightDefined = width && height;
			if(!widthAndHeightDefined) {
				width  = texture.width;
				height = texture.height;
			}
			
			if(rotation) {
				ctx.save();
				
				ctx.translate(x, y);
				ctx.rotate(rotation);
				
				if(widthAndHeightDefined)
					ctx.drawImage(texture, -width*this.pivotX, -height*this.pivotY, width, height);
				else if(this.hint)
					ctx.drawImage(texture, Math.round(-width*this.pivotX), Math.round(-height*this.pivotY));
				else
					ctx.drawImage(texture, -width*this.pivotX, -height*this.pivotY);
				
				ctx.restore();
			} else {
				if(widthAndHeightDefined)
					ctx.drawImage(texture, x-width*this.pivotX, y-height*this.pivotY, width, height);
				else if(this.hint)
					ctx.drawImage(texture, Math.round(x-width*this.pivotX), Math.round(y-height*this.pivotY));
				else
					ctx.drawImage(texture, x-width*this.pivotX, y-height*this.pivotY);
			}
		}
		
		private plannedSetHue: number;
		/** Change the hue of this sprite (from original image), rotation is in radians. */
		public rotateHue(rot: number): void {
			if(!this.complete) {
				this.plannedSetHue = rot;
				return;
			}
			
			this.clearCanvas();
			var ctx = this.asCanvas.getContext("2d");
			
			var rotSin = Math.sin(rot);
			var rotCos = Math.cos(rot);
			
			var hueRotationMatrix = Color.computeHueRotationMatrix(rot);
			
			var imgdata: ImageData;
			try {
				imgdata = ctx.getImageData(0, 0, this.asCanvas.width, this.asCanvas.height);
			} catch(e) {
				console.error(e.message);
				return;
			}
			
			var imgdatalen = imgdata.data.length;
			var color = new Color();
			for(var i=0; i<imgdatalen/4; i++) {
				color.red   = imgdata.data[4*i  ];
				color.green = imgdata.data[4*i+1];
				color.blue  = imgdata.data[4*i+2];
				//color.alpha = imgdata.data[4*i+3];
				
				color.applyMatrix(hueRotationMatrix);
				
				imgdata.data[4*i  ] = color.red;
				imgdata.data[4*i+1] = color.green;
				imgdata.data[4*i+2] = color.blue;
				//imgdata.data[4*i+3] = color.alpha;
			}
			
			ctx.putImageData(imgdata, 0, 0);
		}
		
		
		
		private imageLoaded() : void {
			this.complete = true;
			
			if(this.plannedSetHue !== undefined)
				this.rotateHue(this.plannedSetHue);
		}
		
		private imageError() : void {
			this.failed = true;
		}
		
		private clearCanvas() : void {
			if(!this.asCanvas) {
				this.asCanvas = <HTMLCanvasElement>$('<canvas>').get(0);
				this.asCanvas.width = this.asImage.width;
				this.asCanvas.height = this.asImage.height;
			}
			
			var ctx = this.asCanvas.getContext("2d");
			ctx.clearRect(0, 0, this.asImage.width, this.asImage.height);
			ctx.drawImage(this.asImage, 0, 0);
		}
	}
}
