// <reference path="../MathEx.ts" />

module Graphics {
	
	/** Represents a WebGL shader. */
	export class Shader {
		
		private canvas: Canvas;
		private gl: WebGLRenderingContext;
		private shader: WebGLShader;
		
		/** Construct a shader. */
		public constructor(canvas: Canvas, shaderType: ShaderType, code: string) {
			this.canvas = canvas;
			var gl = this.gl = canvas.webgl;
			
			this.shader = gl.createShader(<number>shaderType);
			gl.shaderSource(this.shader, code);
			gl.compileShader(this.shader);
			gl.VERTEX_SHADER
			
			if(!gl.getShaderParameter(this.shader, gl.COMPILE_STATUS)) {
				console.error(gl.getShaderInfoLog(this.shader));
				return null;
			}
		}
		
		/** INTERNAL FUNCTION, DO NOT USE */
		public _getShader(): WebGLShader {
			return this.shader;
		}
	}
	
	/** Represents a WebGL shader type. */
	export interface ShaderType {}
	
	export module ShaderType {
		
		export var Vertex: ShaderType = WebGLRenderingContext.VERTEX_SHADER;
		
		export var Fragment: ShaderType = WebGLRenderingContext.FRAGMENT_SHADER;
	}
}