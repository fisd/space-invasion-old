module Graphics {

	var webglContexts = ['webgl', 'experimental-webgl', 'webkit-3d', 'moz-webgl'];
	
	/** Represents a space where to render something. */
	export class Canvas {
		
		private canvas: JQuery;
		private c2d: CanvasRenderingContext2D;
		private cgl: WebGLRenderingContext;
		
		/** 2D rendering context. */
		public get context2d(): CanvasRenderingContext2D {
			if(!this.c2d)
				this.c2d = (<HTMLCanvasElement>this.canvas.get(0)).getContext('2d');
			
			return this.c2d;
		}
		
		/** WebGL rendering context. */
		public get webgl(): WebGLRenderingContext {
			if(this.cgl)
				return this.cgl;
			
			var canvas = <HTMLCanvasElement>this.canvas.get(0);
			
			for(var i in webglContexts) {
				try {
					this.cgl = canvas.getContext(webglContexts[i]);
				} catch(e) { }
				
				if(this.cgl) {
					this.initGl();
					return this.cgl;
				}
			}
			
			throw new Error("WebGL not supported");
		}
		
		/** Mouse and keyboard input related to this canvas. */
		public input: Input;
		
		/** Width of the canvas. */
		public width: number;
		
		/** Height of the canvas. */
		public height: number;
		
		/** Seconds passed since last frame was rendered. */
		public deltaTime: number = 0;
		private lastTime: number = new Date().getTime();
		
		public constructor() {
			this.canvas = $('<canvas>').css({
				width: '100%',
				height: '100%',
				background: 'black',
                display: 'block'
			});
			
			this.input = new Input(this.canvas);
			
			// HACK: temporary hack to detect canvas resize
			$(window).resize(() => this.resizeCanvas());
		}
		
		/** Call this function before rendering a frame to clear the buffer and compute time-related data. */
		public newFrame() {
			if(this.c2d) {
				var f = this.c2d.fillStyle;
				this.c2d.fillStyle = 'black';
				this.c2d.fillRect(0, 0, this.width, this.height);
				this.c2d.fillStyle = f;
			} else if(this.cgl) {
				this.cgl.clear(this.cgl.COLOR_BUFFER_BIT | this.cgl.DEPTH_BUFFER_BIT);
			}
			
			var thisTime = new Date().getTime();
			this.deltaTime = (thisTime - this.lastTime) / 1000;
			this.lastTime = thisTime;
		}
		
		/** Insert this canvas into an HTML element. */
		public appendTo(el: JQuery|Element): void {
			this.canvas.appendTo(el);
			
			this.resizeCanvas();
		}
		
		/** Toggle fullscreen mode. */
		public toggleFullscreen(): void {
			this.setFullscreen(!this.isFullscreen());
		}
		
		/** Enter fullscreen mode. */
		public setFullscreen(): void;
		/** Enter/exit fullscreen mode. */
		public setFullscreen(fullscreen: boolean): void;
		setFullscreen(fullscreen?: boolean): void {
			var canvas = <any>this.canvas.get(0);
			
			if(arguments.length == 0)
				fullscreen = true;
			
			if(fullscreen) {
				if(canvas.requestFullscreen)
					canvas.requestFullscreen();
				else if(canvas.webkitRequestFullscreen)
					canvas.webkitRequestFullscreen();
				else if(canvas.msRequestFullscreen)
					canvas.msRequestFullscreen();
				else if(canvas.mozRequestFullScreen)
					canvas.mozRequestFullScreen();
			} else {
				if(canvas.exitFullscreen)
					canvas.exitFullscreen();
				else if(canvas.webkitExitFullscreen)
					canvas.webkitExitFullscreen();
				else if(canvas.msExitFullscreen)
					canvas.msExitFullscreen();
				else if(canvas.mozCancelFullScreen)
					canvas.mozCancelFullScreen();
			}
		}
		
		/** Detect if this canvas is fullscreen. */
		public isFullscreen(): boolean {
			var canvas = <any>this.canvas.get(0);
			var document = <any>window.document;
			
			if(document.fullscreenElement !== undefined)
				return document.fullscreenElement == canvas;
			if(document.webkitFullscreenElement !== undefined)
				return document.webkitFullscreenElement == canvas;
			if(document.msFullscreenElement !== undefined)
				return document.msFullscreenElement == canvas;
			if(document.mozFullScreenElement !== undefined)
				return document.mozFullScreenElement == canvas;
		}
		
		
		
		private resizeCanvas(): void {
			this.width = this.canvas.width();
			this.height = this.canvas.height();
			
			this.canvas.prop('width', this.width);
			this.canvas.prop('height', this.height);
			
			if(this.cgl)
				this.resizeGl();
		}
		
		private resizeGl(): void {
			this.cgl.viewport(0, 0, this.width, this.height);
		}
		
		private initGl(): void {
			this.cgl.clearColor(0, 0, 0, 1);
			
			this.resizeGl();
		}
	}
}
