module Graphics {
	
	import Matrix4 = MathEx.Matrix4;
	
	/** Represents a WebGL uniform. */
	export class Uniform {
		
		private canvas: Canvas;
		private gl: WebGLRenderingContext;
		private program: WebGLProgram;
		
		private location: WebGLUniformLocation;
		private val: any;
		
		/** Extract the specified uniform from the specified program. */
		public constructor(canvas: Canvas, program: Program, name: string) {
			this.canvas = canvas;
			var gl = this.gl = canvas.webgl;
			this.program = program;
			
			this.location = gl.getUniformLocation(program._program, name);
		}
		
		/** Set this uniform to a Matrix4. */
		public get matrix4(): Matrix4 {
			return this.val;
		}
		public set matrix4(value: Matrix4) {
			this.gl.uniformMatrix4fv(this.location, false, this.val = <any>value);
		}
	}
}