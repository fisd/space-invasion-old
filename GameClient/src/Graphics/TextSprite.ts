module Graphics {
	
	/** Represents a renderizable string. */
	export class TextSprite implements Sprite {
		
		private canvas: Canvas;
		
		/** Text to be rendered. */
		public text: string;
		
		/** Font family. */
		public fontFamily: string = 'monospace';
		/** Font size in pixels. */
		public fontSize: number = 14;
		/** Font color. */
		public fontColor: Color = Color.white;
		
		/** Horizontal alignment. */
		public horizontalAlignment = HorizontalAlignment.left;
		/** Horizontal alignment. */
		public verticalAlignment = VerticalAlignment.top;
		
		/** Construct a text sprite to be rendered in canvas. */
		public constructor(canvas: Canvas) {
			this.canvas = canvas;
		}
		
		/** Render this sprite at the specified coordinates. */
		public render(x: number, y: number): void;
		
		/** Render this sprite with the specified coordinates and rotation in radians. */
		public render(x: number, y: number, rotation: number): void;
		
		/** Render this sprite with the specified coordinates and dimension. */
		public render(x: number, y: number, width: number, height: number): void;
		
		/** Render this sprite with the specified coordinates, dimension, and rotation in radians. */
		public render(x: number, y: number, width: number, height: number, rotation: number): void;
		
		render(x: number, y: number, widthOrRotation?: number, height?: number, rotation?: number): void {
			var width = <number>undefined;
			if(arguments.length == 3)
				rotation = widthOrRotation;
			else
				width = widthOrRotation;
			
			var ctx = this.canvas.context2d;
			
			ctx.save();
			
			if(rotation) {
				ctx.translate(x, y);
				ctx.rotate(rotation);
				
				this.lowLeverRender(0, 0, width);
			} else {
				this.lowLeverRender(x, y, width);
			}
			
			ctx.restore();
		}
		
		
		
		private lowLeverRender(x: number, y: number, maxWidth?: number): void {
			var ctx = this.canvas.context2d;
			
			var s = ctx.measureText(this.text);
		    ctx.font = this.fontSize + 'px ' + this.fontFamily;
			ctx.fillStyle = this.fontColor;
		    ctx.fillText(
				this.text,
				
				x - (this.horizontalAlignment+1) * s.width       / 2,
				y - (this.verticalAlignment  +1) * this.fontSize / 2,
				
				maxWidth
			);
		}
	}
}