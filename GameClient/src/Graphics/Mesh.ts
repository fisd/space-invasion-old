// <reference path="../MathEx.ts" />

module Graphics {

	import Vector3 = MathEx.Vector3;
	
	/** Represents data required to render a WebGL mesh, not processed. */
	export interface MeshData {
		
		/** Mesh vertices. */
		vertices: Vector3[];
		
		/** Mesh triangle indices. */
		triangles: number[];
	}
	
	/** INTERNAL INTERFACE, DO NOT USE */
	export interface _WebGLMeshData {
		
		_verticesBuffer: WebGLBuffer;
		_trianglesBuffer: WebGLBuffer;
	}
	
	/** Represents a WebGL mesh. */
	export class Mesh implements MeshData, _WebGLMeshData {
		
		private canvas: Canvas;
		private gl: WebGLRenderingContext;
		private drawMode: number;
		
		/** INTERNAL PROPERTY, DO NOT USE */
		public _verticesBuffer: WebGLBuffer;
		private _optimizedVertices: Float32Array;
		private _vertices: Vector3[];
		/** Mesh vertices. */
		public get vertices(): Vector3[] {
			return this._vertices;
		}
		public set vertices(v: Vector3[]) {
			this._vertices = v;
			this.regenerateVertices();
		}
		
		/** INTERNAL PROPERTY, DO NOT USE */
		public _trianglesBuffer: WebGLBuffer;
		private _optimizedTriangles: Uint16Array;
		private _triangles: number[];
		/** Mesh triangle indices. */
		public get triangles(): number[] {
			return this._triangles;
		}
		public set triangles(v: number[]) {
			this._triangles = v;
			this.regenerateTriangles();
		}
		
		public _colorsBuffer: WebGLBuffer;
		private _optimizedColors: Float32Array;
		private _colors: Color[];
		/** Mesh colors. */
		public get colors(): Color[] {
			return this._colors;
		}
		public set colors(v: Color[]) {
			this._colors = v;
			this.regenerateColors();
		}
		
		
		
		/** Construct an empty mesh. */
		public constructor(canvas: Canvas, dynamic: boolean = false) {
			this.canvas = canvas;
			var gl = this.gl = canvas.webgl;
			this.drawMode = dynamic ? gl.DYNAMIC_DRAW : gl.STATIC_DRAW;
			
			this._verticesBuffer  = gl.createBuffer();
			this._trianglesBuffer = gl.createBuffer();
			this._colorsBuffer    = gl.createBuffer();
		}
		
		
		
		private regenerateVertices(): void {
			var gl = this.gl;
			
			if(!this._optimizedVertices || this._optimizedVertices.length != this._vertices.length * 3)
				this._optimizedVertices = new Float32Array(this._vertices.length * 3);
			
			for(var i=0; i<this._vertices.length; i++) {
				var vertex = this._vertices[i];
				
				for(var j=0; j<3; j++)
					this._optimizedVertices[i*3+j] = vertex[j];
			}
			
			gl.bindBuffer(gl.ARRAY_BUFFER, this._verticesBuffer);
			gl.bufferData(gl.ARRAY_BUFFER, this._optimizedVertices, this.drawMode);
		}
		
		private regenerateTriangles(): void {
			var gl = this.gl;
			
			if(!this._optimizedTriangles || this._optimizedTriangles.length != this._triangles.length)
				this._optimizedTriangles = new Uint16Array(this._triangles.length);
			
			for(var i=0; i<this._triangles.length; i++)
				this._optimizedTriangles[i] = this._triangles[i];
			
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._trianglesBuffer);
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this._optimizedTriangles, this.drawMode);
		}
		
		private regenerateColors(): void {
			if(!this._colors) {
				this._optimizedColors = null;
				gl.bindBuffer(gl.ARRAY_BUFFER, this._colorsBuffer);
				gl.bufferData(gl.ARRAY_BUFFER, this._optimizedColors, this.drawMode);
				return;
			}
			
			var gl = this.gl;
			
			if(!this._optimizedColors || this._optimizedColors.length != this._colors.length * 4)
				this._optimizedColors = new Float32Array(this._colors.length * 4);
			
			for(var i=0; i<this._colors.length; i++) {
				var color = this._colors[i];
				this._optimizedColors[i*4  ] = color.red   / 255;
				this._optimizedColors[i*4+1] = color.green / 255;
				this._optimizedColors[i*4+2] = color.blue  / 255;
				this._optimizedColors[i*4+3] = color.alpha / 255;
			}
			
			gl.bindBuffer(gl.ARRAY_BUFFER, this._colorsBuffer);
			gl.bufferData(gl.ARRAY_BUFFER, this._optimizedColors, this.drawMode);
		}
	}
}