module Graphics {
	
	/** Represents a renderizable sprite. */
	export interface Sprite {
		
		/** Render this sprite at the specified coordinates. */
		render(x: number, y: number): void;
		
		/** Render this sprite with the specified coordinates and rotation in radians. */
		render(x: number, y: number, rotation: number): void;
		
		/** Render this sprite with the specified coordinates and dimension. */
		render(x: number, y: number, width: number, height: number): void;
		
		/** Render this sprite with the specified coordinates, dimension, and rotation in radians. */
		render(x: number, y: number, width: number, height: number, rotation: number): void;
	}
}