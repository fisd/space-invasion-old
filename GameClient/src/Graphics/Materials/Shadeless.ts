
module Graphics.Materials {
	
	var vertexShader = 
`// VERTEX SHADER

attribute vec3 aVertexPosition;
attribute vec4 aVertexColor;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

varying lowp vec4 vColor;

void main(void) {
	vColor = aVertexColor;
	gl_Position = pMatrix * mvMatrix * vec4(aVertexPosition, 1.0);
}
`;
	
	var fragmentShader = 
`// FRAGMENT SHADER

precision mediump float;

varying lowp vec4 vColor;

void main(void) {
	gl_FragColor = vColor;
}
`;

	import Matrix4 = MathEx.Matrix4;	
	
	/** Shade-less, monochromatic material. */
	export class Shadeless implements Material {
		
		private canvas: Canvas;
		private gl: WebGLRenderingContext;
		
		private vertexShader: Shader;
		private fragmentShader: Shader;
		
		private program: Program;
		
		private mvMatrixUniform: Uniform;
		private pMatrixUniform: Uniform;
		
		private aVertexPosition: Attribute;
		private aVertexColor: Attribute;
		
		public get mvMatrix(): Matrix4 {
			return this.mvMatrixUniform.matrix4;
		}
		public set mvMatrix(v: Matrix4) {
			this.mvMatrixUniform.matrix4 = v;
		}
		
		public get pMatrix(): Matrix4 {
			return this.pMatrixUniform.matrix4;
		}
		public set pMatrix(v: Matrix4) {
			this.pMatrixUniform.matrix4 = v;
		}
		
		public constructor(canvas: Canvas) {
			this.canvas = canvas;
			var gl = this.gl = canvas.webgl;
			
			this.vertexShader   = new Shader(canvas, ShaderType.Vertex, vertexShader);
			this.fragmentShader = new Shader(canvas, ShaderType.Fragment, fragmentShader);
			
			this.program = new Program(canvas, [this.vertexShader, this.fragmentShader]);
			
			this.mvMatrixUniform = new Uniform(canvas, this.program, "mvMatrix");
			this.pMatrixUniform  = new Uniform(canvas, this.program, "pMatrix");
			
			this.aVertexPosition = new Attribute(canvas, this.program, "aVertexPosition");
			this.aVertexColor    = new Attribute(canvas, this.program, "aVertexColor");
			
			this.aVertexPosition.enable();
			this.aVertexColor.enable();
		}
		
		public use(): void {
			this.program.use();
		}
		
		public render(mesh: Mesh): void {
			this.gl.bindBuffer(this.gl.ARRAY_BUFFER, mesh._verticesBuffer);
			this.aVertexPosition._setPointer(3, this.gl.FLOAT, false, 0, 0);
			
			this.gl.bindBuffer(this.gl.ARRAY_BUFFER, mesh._colorsBuffer);
			this.aVertexColor._setPointer(4, this.gl.FLOAT, true, 0, 0);
			
			this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, mesh._trianglesBuffer);
			this.gl.drawElements(this.gl.TRIANGLES, mesh.triangles.length, this.gl.UNSIGNED_SHORT, 0);
			//this.gl.drawArrays(this.gl.TRIANGLES, 0, mesh.triangles.length);
		}
	}
}