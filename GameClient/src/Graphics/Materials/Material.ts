
module Graphics.Materials {
	
	import Matrix4 = MathEx.Matrix4;
	
	/** Represents a WebGL material. */
	export interface Material {
		
		/** Model translation matrix. */
		mvMatrix: Matrix4;
		
		/** Projection matrix. */
		pMatrix: Matrix4;
		
		/** Indicate WebGL to use this material. */
		use(): void;
		
		/** Render a mesh with this material. */
		render(mesh: Mesh): void;
	}
}