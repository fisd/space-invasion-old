module Graphics {

	/** Represents input related to an HTML element. */
	export class Input {
		
		private canvas: JQuery;
		
		/** Mouse position. */
		public mouse = new MathEx.Vector2();
		
		/** Mouse buttons status. */
		public button: { [button: number]: boolean } = {};
		
		/** Keyboard keys status. */
		public key: { [key: number]: boolean } = {};
		
		/** Holds functions called when a key is pressed, max one function per-key. */
		public keyHandler: { [key: number]: (down?: boolean, key?: number) => void } = {};
		
		/** Construct an input manager for the specified element. */
		public constructor(canvas: JQuery) {
			this.canvas = canvas;
			
			$(canvas).bind('contextmenu', () => false );
			
			$(window)
				.mousemove((e) => this.mouseMoved(e))
				.mousedown((e) => this.buttonDown(e))
				.mouseup  ((e) => this.buttonUp(e)  )
				.keydown  ((e) => this.keyDown(e)   )
				.keyup    ((e) => this.keyUp(e)     );
		}
		
		
		
		private mouseMoved(e: JQueryMouseEventObject): void {
		   var offset = this.canvas.offset(); 
		   var x = e.pageX - offset.left;
		   var y = e.pageY - offset.top;
			
			if(x < 0)
				x = 0;
			else if(x > this.canvas.width())
				x = this.canvas.width();
			
			if(y < 0)
				y = 0;
			else if(y > this.canvas.height())
				y = this.canvas.height();
			
			this.mouse.x = x;
			this.mouse.y = y;
		}
		
		private buttonDown(e: JQueryMouseEventObject): void {
			this.mouseMoved(e);
			
			this.button[e.button] = true;
		}
		
		private buttonUp(e: JQueryMouseEventObject): void {
			this.mouseMoved(e);
			
			this.button[e.button] = false;
		}
		
		private keyDown(e: JQueryKeyEventObject): void {
			this.key[e.which] = true;
			
			if(this.keyHandler[e.which]) {
				this.keyHandler[e.which](true, e.which);
				//e.preventDefault();
			}
		}
		
		private keyUp(e: JQueryKeyEventObject): void {
			this.key[e.which] = false;
			
			if(this.keyHandler[e.which]) {
				this.keyHandler[e.which](false, e.which);
				//e.preventDefault();
			}
		}
	}
}
