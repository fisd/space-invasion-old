/// <reference path="../Graphics.ts" />

module Game {

	import Canvas = Graphics.Canvas;
	
	/** Represents a map. */
	export class Map {
		
		private canvas: Canvas;
		
		private planets: Planet[];
		
		private ships: Ship[] = [];
		
		private particles: Particle[] = [];
		
		/** Construct a map with the specified properties. */
		public constructor(canvas: Canvas, planets: Planet[]) {
			this.canvas = canvas;
			
			this.planets = planets;
		}
		
		/** Render this map. */
		public render(): void {
			for(var i in this.planets)
				this.planets[i].render();
			
			for(var i in this.particles)
				this.particles[i].render();
			
			for(var i in this.ships)
				this.ships[i].render();
		}
		
		/** Proceed with the simulation (time extracted from canvas). */
		public tick(): void {
			var i: any;
			
			for(i in this.planets)
				this.planets[i].tick();
			
			for(i in this.ships)
				this.ships[i].tick();
				
			for(i in this.particles)
				this.particles[i].tick();
			
			i = this.ships.length;
			while(i--)
				if(this.ships[i].dead)
					this.ships.splice(i, 1);
			
			i = this.particles.length;
			while(i--)
				if(this.particles[i].dead)
					this.particles.splice(i, 1);
		}
		
		/** Add a ship to this map. */
		public addShip(ship: Ship): void {
			this.ships.push(ship);
		}
		
		/** Add a particle to this map. */
		public addParticle(particle: Particle): void {
			this.particles.push(particle);
		}
	}
}