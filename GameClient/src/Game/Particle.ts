module Game {
	
	/** Represents a small particle. */
	export class Particle {
		
		private canvas: Graphics.Canvas;
		private sprite: Graphics.ImageSprite;
		
		/** Rigidbody of this particle. */
		public rigidbody: Physics.Rigidbody;
		
		private maxLife: number;
		private life: number;
		
		/** True if the particle is dead, readonly. */
		public dead: boolean = false;
		
		/** Construct a particle with the specified paramenters. */
		public constructor(canvas: Graphics.Canvas, sprite: Graphics.ImageSprite, position: MathEx.Vector2, velocity: MathEx.Vector2, life: number) {
			this.canvas = canvas;
			this.sprite = sprite;
			
			this.rigidbody = new Physics.Rigidbody();
			this.rigidbody.position = position;
			this.rigidbody.velocity = velocity;
			
			this.maxLife = life;
			this.life = life;
		}
		
		/** Render this particle. */
		public render(): void {
			this.canvas.context2d.globalAlpha = MathEx.lerp(0, 1, this.life/this.maxLife);
			
			this.sprite.render(
				this.rigidbody.position.x,
				this.rigidbody.position.y,
				this.rigidbody.rotation
			);
			
			this.canvas.context2d.globalAlpha = 1;
		}
		
		/** Proceed with the simulation (time extracted from canvas). */
		public tick(): void {
			this.rigidbody.simulate(this.canvas.deltaTime);
			
			this.life -= this.canvas.deltaTime;
			
			if(this.life <= 0)
				this.dead = true;
		}
	}
}