/// <reference path="../MathEx.ts" />

module Game {
	
	import Vector2 = MathEx.Vector2;
	
	/** Represents a space ship. */
	export class Ship {
		
		private map: Map;
		private canvas: Graphics.Canvas;
		private sprite: Graphics.ImageSprite;
		
		/** Rigidbody of this ship. */
		public rigidbody: Physics.Rigidbody;
		
		/** True if the particle is dead, readonly. */
		public dead: boolean = false;
		
		private nextParticle = 0;
		
		/** Construct a ship with the specified parameters. */
		constructor(map: Map, canvas: Graphics.Canvas, sprite: Graphics.ImageSprite, position: Vector2) {
			this.map = map;
			this.canvas = canvas;
			this.sprite = sprite;
			
			this.rigidbody = new Physics.Rigidbody();
			this.rigidbody.position = position;
		}
		
		/** Render this ship. */
		public render(): void {
			this.sprite.render(this.rigidbody.position.x, this.rigidbody.position.y, this.rigidbody.rotation);
		}
		
		/** Proceed with the simulation (time extracted from canvas). */
		public tick(): void {
			var THRUST = 3;
			
			var propulsion = new Vector2(
				 Math.sin(this.rigidbody.rotation),
				-Math.cos(this.rigidbody.rotation)
			).multiplyBy(THRUST);
			
			//this.rigidbody.addForce(propulsion, this.canvas.deltaTime);
			this.rigidbody.simulate(this.canvas.deltaTime);
			
			// test
			var testSprite = new Graphics.ImageSprite(this.canvas, Textures.test.particle);
			
			this.nextParticle += this.canvas.deltaTime * THRUST * 5;
			
			while(this.nextParticle >= 1) {
				this.nextParticle--;
				
				var p = new Particle(
					this.canvas,
					testSprite,
					this.rigidbody.position,
					new Vector2(
						(Math.random()*6 - 3) - propulsion.x * 10,
						(Math.random()*6 - 3) - propulsion.y * 10
					).add(this.rigidbody.velocity),
					3
				);
				
				this.map.addParticle(p);
			}
		}
	}
}