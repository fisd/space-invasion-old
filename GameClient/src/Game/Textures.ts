module Game {
	
	/** Contains the textures used by the game. */
	export module Textures {
		
		export var test = {
			square:   img("res/img/test/square.png"),
			ship:     img("res/img/test/ship.png"),
			rgb:      img("res/img/test/rgb.png"),
			particle: img("res/img/test/particle.png")
		};
		
		export var planets = {
			clouds: img("res/img/planets/clouds.png"),
			earth:  img("res/img/planets/earth.png"),
			shadow: img("res/img/planets/shadow.png")
		};
		
		export var gui = {
			cursorArrow: img("res/img/gui/cursor_arrow.png"),
			cursorCross: img("res/img/gui/cursor_cross.png")
		};
		
		
		function img(src: string): HTMLImageElement {
			var img = new Image();
			img.src = src;
			
			return img;
		}
	}
}