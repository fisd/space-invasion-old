/// <reference path="../MathEx.ts" />
/// <reference path="../Graphics.ts" />

/** Game module. */
module Game {
	
	import Matrix4 = MathEx.Matrix4;
	import Vector2 = MathEx.Vector2;
	import Vector3 = MathEx.Vector3;
	
	import Canvas = Graphics.Canvas;
	import Color = Graphics.Color;
	import ImageSprite = Graphics.ImageSprite;
	import Materials = Graphics.Materials;
	import Mesh = Graphics.Mesh;
	import Program = Graphics.Program;
	import Shader = Graphics.Shader;
	import ShaderType = Graphics.ShaderType;
	import Uniform = Graphics.Uniform;
	
	/** Main class. */
	export class Main
	{
		private canvas: Canvas;	
		
		/** Entry point. */
		public static main(): void {
			var canvas = new Canvas();
			canvas.appendTo(document.body);
			
			canvas.input.keyHandler[122] = (down, key) => {	// F11
				if(down)
					canvas.toggleFullscreen();
			}; 
			
			// === WEBGL TEST ===
			var testMaterial = new Materials.Shadeless(canvas);
			
			var testMesh = new Mesh(canvas);
			testMesh.vertices = [
				new Vector3( 3,  3,  3), // 0
				new Vector3(-3,  3,  3), // 1
				new Vector3( 3, -3,  3), // 2
				new Vector3(-3, -3,  3), // 3
				new Vector3( 3,  3, -3), // 4
				new Vector3(-3,  3, -3), // 5
				new Vector3( 3, -3, -3), // 6
				new Vector3(-3, -3, -3)  // 7
			];
			testMesh.triangles = [
				0, 2, 3,
				0, 3, 1,
				2, 6, 7,
				2, 7, 3,
				4, 7, 6,
				4, 5, 7,
				0, 1, 5,
				0, 5, 4,
				1, 3, 7,
				1, 7, 5,
				0, 4, 6,
				0, 6, 2
			];
			testMesh.colors = [
				Color.white,
				Color.cyan,
				Color.magenta,
				Color.blue,
				Color.yellow,
				Color.green,
				Color.red,
				Color.black
			];
			
			testMaterial.use();
			
			var resizeFunc = () => {
				testMaterial.pMatrix = new Matrix4().toPerspective(
					120,
					canvas.width/canvas.height,
					0.1,
					1000
				);
			};
			$(window).resize(resizeFunc);
			resizeFunc();
			
			testMaterial.mvMatrix = new Matrix4();
			
			canvas.webgl.enable(canvas.webgl.DEPTH_TEST);
			
			var rot = 0;
			setInterval(() => {
				canvas.newFrame();
				
				rot += canvas.deltaTime * Math.PI;
				
				testMaterial.mvMatrix = testMaterial.mvMatrix
					.toIdentity()
					.translateBy(new Vector3(0, 0, -20))
					.rotateX(Math.sin(rot), Math.cos(rot))
					.rotateY(Math.sin(rot/2), Math.cos(rot/2))
					.rotateZ(Math.sin(rot/3), Math.cos(rot/3))
				;
					
				testMaterial.render(testMesh);
			}, 1000 / 60);
			
			
			
			//// === CONTEXT 2D TEST ===
			//
			//var testSprite = new ImageSprite(canvas, Textures.test.ship);
			//testSprite.pivotX = 0.5;
			//testSprite.pivotY = 1;
			//
			//var earthSprite  = new ImageSprite(canvas, Textures.planets.earth );
			//var cloudsSprite = new ImageSprite(canvas, Textures.planets.clouds);
			//var shadowSprite = new ImageSprite(canvas, Textures.planets.shadow);
			//
			//var earth = new Planet(canvas, [earthSprite, cloudsSprite, shadowSprite], new Vector2(150, 150), 150);
			//
			//var arrowCursor = new ImageSprite(canvas, Textures.gui.cursorArrow);
			//arrowCursor.hint = true;
			//arrowCursor.pivotX = 0.27;
			//arrowCursor.pivotY = 0.27;
			//
			//var crossCursor = new ImageSprite(canvas, Textures.gui.cursorCross);
			//
			//arrowCursor.rotateHue(120 * Math.PI/180);
			//crossCursor.rotateHue(120 * Math.PI/180);
			//
			//var map = new Map(canvas, [earth]);
			//
			//var testShip = new Ship(map, canvas, testSprite, new Vector2(400, 100));
			////testShip.rigidbody.velocity.x = 10;
			//
			//map.addShip(testShip);
			//
			//var rgbtest = new ImageSprite(canvas, Textures.test.rgb);
			//rgbtest.pivotX = 0;
			//rgbtest.pivotY = 0;
			//
			//var time = 0;
			//setInterval(() => {
			//	map.tick();
			//	
			//	canvas.newFrame();
			//	map.render();
			//	
			//	time += canvas.deltaTime;
			//	rgbtest.rotateHue(time);
			//	rgbtest.render(0, 0);
			//	
			//	testShip.rigidbody.angularMomentum += canvas.deltaTime * 0.01;
			//	
			//	(canvas.input.button[0] ? crossCursor : arrowCursor).render(canvas.input.mouse.x, canvas.input.mouse.y);
			//}, 1000 / 60);
		}
	}
}
