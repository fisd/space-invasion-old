/// <reference path="../MathEx.ts" />
/// <reference path="../Graphics.ts" />

module Game {
	
	import Vector2 = MathEx.Vector2;
	
	import Canvas = Graphics.Canvas;
	import ImageSprite = Graphics.ImageSprite;
	import TextSprite = Graphics.TextSprite;
	import HorizontalAlignment = Graphics.HorizontalAlignment;
	import VerticalAlignment = Graphics.VerticalAlignment;
	
	/** Represents a planet. */
	export class Planet {
		
		private canvas: Canvas;
		
		private sprites: ImageSprite[];
		private text: TextSprite;
		
		private position: Vector2;
		
		private maxPopulation: number;
		private planetSize: number;
		
		/** Current population of the planet. */
		public population: number = 0;
		
		/** Increment in population every second. */
		public repopulationRate: number;
		private repopulationExtra = 0;
		
		/** Construct a planet with the specified properties. */
		public constructor(canvas: Canvas, sprites: ImageSprite[], position: Vector2, maxPopulation: number) {
			this.canvas = canvas;
			
			this.sprites = sprites;
			
			this.text = new TextSprite(canvas);
			this.text.horizontalAlignment = HorizontalAlignment.center;
			this.text.verticalAlignment = VerticalAlignment.top;
			
			this.position = position;
			
			this.maxPopulation = maxPopulation;
			this.repopulationRate = maxPopulation/100;
			
			this.planetSize = Math.sqrt(this.maxPopulation) * 10;
		}
		
		/** Render this planet. */
		public render(): void {
			for(var i in this.sprites)
				this.sprites[i].render(this.position.x, this.position.y, this.planetSize, this.planetSize);
			
			this.text.text = this.population.toString();
			this.text.render(this.position.x, this.position.y + this.planetSize/2 + 20);
		}
		
		/** Proceed with the simulation (time extracted from canvas). */
		public tick(): void {
			this.repopulationExtra += this.canvas.deltaTime * this.repopulationRate;
			
			var increment = Math.floor(this.repopulationExtra);
			this.population += increment;
			this.repopulationExtra -= increment;
			
			if(this.population >= this.maxPopulation) {
				this.population = this.maxPopulation;
				this.repopulationExtra = 0;
			}
		}
	}
}