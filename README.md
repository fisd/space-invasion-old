# Space Invasion

Space Invasion is a work-in-progress with the intent to become a simple strategy
game where players have to battle to conquer a portion of space by distributing
armies and colonizing planets.

This project will be developed with a custom game engine, written in parallel to
accomodate game requirements.

## Installing

Open the project with Eclipse and compile.

## Usage

After installing, run `index.html` with a modern browser.

## Contributing

1. Fork the repository at https://bitbucket.org/bruce965/space-invasion.git
2. Download and install [Eclipse](https://www.eclipse.org)
3. Install [Eclipse Typescript](https://github.com/palantir/eclipse-typescript)
4. Open this project with Eclipse

## Credits

Game and graphics by Fabio Iotti.

## License

Read `LICENSE.txt` before using this software.
